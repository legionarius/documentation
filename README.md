<iframe frameborder="0" src="https://gitlab.com/legionarius/ressources/graphic-identity/-/blob/5c4ec1cfc20d3698bd200e7b4ef665705426394b/Logo/facebook_cover_photo_2.png">Legionarius</iframe>

# M2 - Project GameJam - Legionarius

## Contexte

Projet libre de 300h

## Equipe

- Adrien C
- Bogdan C
- Romain M

## Objectifs

- Développer nos compétences de programmation en utilisant comme support le jeu vidéo.
- Eprouver nos réalisations au cours de game-jam.
- Vivre la chaîne de production d'un jeu vidéo ( Conception, réalisation, déploiement, commercialisation )

## Technologies

Utilisation d'un moteur de jeu : Godot

- Moteur Opensource, pas de royalties.
- Environnment de développement léger.
- Crossplatform.
- Possiblité d'utilisation de plusieurs langagues ( Language natif GDScript pour du POC de fonctionnalitée, C++ ).

## Ressources

Documentation Gitbook : https://legionarius.gitlab.io/documentation/  
Storyboard : https://miro.com/app/dashboard/  
Moteur de jeu : https://godotengine.org/  
Plateforme d'hébergement : https://itch.io/  
Plateforme de financement : https://www.patreon.com/
