# GameJam Report

## Weekly GameJam 190

Thème : Walk on Walls  
https://itch.io/jam/weekly-game-jam-190

Date : 26 Février 2021 au 4 MArs 2021

### Contexte

Développement d'un jeu vidéo sur un thème et une période donnée.

### Phase de développement

- Plannification
- Découpage des tâches ( Issue Gitlab )
- Storyboarding/brainstorming : https://miro.com/app/board/o9J_lSTtvxQ=/
- Développement : https://gitlab.com/legionarius/weekly-gamejam-190

### Résultats

Itchio page : https://itch.io/jam/weekly-game-jam-190/entries  
Equipe : Legionarius - Alien Abduction  
Page du jeu : https://octogene.itch.io/alien-abduction

<iframe frameborder="0" src="https://itch.io/embed/944839" width="552" height="167"><a href="https://octogene.itch.io/alien-abduction">Alien Abduction by octogene, acroquelois, Nahalu</a></iframe>

### Objectifs

- Retour sur la première GameJam - Retex
- Amélioration des workflows

### Prochain objectifs

- Réparer la CI
- Modifier la CI pour builder en HTML5
- Support compilation database
- Polissage du jeu développé ( Transition de niveau )
- Etendre les fonctionnalités développées
- Améliorer les prioritées de développement
- Création de l'identité graphique ( https://www.flickr.com/commons )

## MumdaneJam

Thème : Decay
https://itch.io/jam/mundane-jam

Date : 12 Mars 2021 au 22 MArs 2021

### Contexte

Développement d'un jeu vidéo sur un thème et une période donnée.

### Phase de développement

- Plannification
- Découpage des tâches ( Issue Gitlab )
- Storyboarding/brainstorming :
- Développement : https://gitlab.com/legionarius/mundane-jam

### Résultats

Itchio page : https://itch.io/jam/mundane-jam/entries
Equipe : Legionarius - Landlord
Page du jeu : https://octogene.itch.io/landlord

<iframe frameborder="0" src="https://itch.io/embed/967237" width="552" height="167"><a href="https://octogene.itch.io/landlord">Landlord by octogene, acroquelois, Nahalu</a></iframe>

### Objectifs

- Retour sur la première GameJam - Retex
- Amélioration des workflows

### Prochain objectifs

- Réparer le build Windows CI
- Modifier la CI pour builder en HTML5
- Changer la résolution en 16:9
- Améliorer l'écran de menu ( Controls, settings )
- Implémenter un écran de pause
