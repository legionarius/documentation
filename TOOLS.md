# TOOLS

## IDE

**Clion**

## Assets

### Sounds

* **SFXR**
* **Ableton**
* **Audacity**

### Graphics

* **Aseprite**
* **Gimp**
* **Blender**

## Conception

* **Miro**(white board)
