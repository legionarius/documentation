# Guidelines

## Godot

### Directory structure

Below the directory structure of a Godot project:

```
Project
│
└───entity
│   │
│   └───Player
│   │   │ Player.tscn
│   │   │ Player.gdns
│   │
│   └───Map
│       │ Map.tscn
│
└───src
│   │ Player.cpp
│
└───lib
│   │ Nat.so
│
└───asset
    │
    └───Player
        │
        └─── gfx
        │    │ player_jump0.png
        │
        └─── sound
        │    │ player_jump.ogg
        │
        └─── font
             │ Valentime.otf
```

### Version

- Godot 3.2

Documentation link: https://docs.godotengine.org/en/stable/index.html

### Naming convention

Scene: `<Name>`  
_Example: Player.tscn_

Script: `<Name>`  
_Example: Player.cpp_

Parent node: `<Name>`  
_Example: Player_

Child node: `<Name><NodeType>`  
_Example: PlayerSpriteAnimation_

Asset: `<name>_<description>`  
_Example: player_jump.ogg, player_jump0.png, player_jump1.png_

Methods: Add **\_** as prefix for private methods

## CPP

### Version

- C++ 20
- Clang v11

### Naming convention

- Standard C++ 20

Linter enforcement link: https://github.com/cpplint/cpplint  
Styleguide link: https://google.github.io/styleguide/cppguide.html

## Git

### Branch

1 Branch Master -> feat/ fix/

### Naming convention

Git conventional commits

### Merge request process

**2 Reviewers must approve the MR**

An MR have 4 steps to be validated:  
1 - Playtest  
2 - Tweaks (Optional)  
3 - Code review  
4 - Merge

## Game-jam documentation

- Context
- Project's choice
- Recap'

## Plannification

Gitlabboard
